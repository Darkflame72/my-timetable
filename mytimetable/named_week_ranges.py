# Use `cal -wm currentyear` to view week numbers

named_week_ranges = {
    "Trimester 1": [(7, 13), (15, 20)],
    "Trimester 1 (first 6 weeks)": [(7, 13)],
    "Trimester 1 (last 6 weeks)":  [(15, 20)],
    "Trimester 1 (starts 2nd week)": [(8, 13), (15, 20)],

    "Trimester 2": [(25, 30), (33, 38)],
    "Trimester 2 (first 6 weeks)": [(25, 30)],
    "Trimester 2 (last 6 weeks)":  [(33, 38)],
    "Trimester 2 (starts 2nd week)": [(26, 30), (33, 38)],

    "Trimester 1 + 2": [(7, 13), (15, 20), (25, 30), (33, 38)]
}
