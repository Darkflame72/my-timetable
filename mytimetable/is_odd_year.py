from datetime import datetime


def is_odd_year():
    return datetime.now().year % 2
