# Copyright (C) 2017  Jamie McClymont, Rhys Davies, and Nick Webster
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from mytimetable.datatypes import KeyDates
from mytimetable.util import grouper

from bs4 import BeautifulSoup
import itertools
import datetime
import re

month_year_re = re.compile(r"^([A-Z][a-z]+)\s*(\d{4})$")
dom_re = re.compile(r"\d+$")

closed_re = re.compile(r"(university closed|no lectures)", re.IGNORECASE)  # TODO Are we *sure* that "no lectures" means there *aren't* tutorials?

trimester_re_base = r"trimester\s*TRIMESTERNUM\s*BEGINEND"

trimester_res = {}
for tri_n in range(1, 4):
    re_ = trimester_re_base.replace("TRIMESTERNUM", str(tri_n))

    trimester_res[tri_n] = (
        re.compile(re_.replace("BEGINEND", "begins"), re.I),
        re.compile(re_.replace("BEGINEND", "ends|teaching period ends"), re.I) if tri_n != 3 else None
    )


months_of_year = \
    [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    ]


def prettify_key_date_text(text):
    return re.sub(r"\s+", " ", text).strip()


def scrape(html):
    doc = BeautifulSoup(html, 'html.parser')

    first_h3 = doc.find("div", class_="block formatting").find("h3")

    events = []

    months_and_tables = filter(lambda html_data: len(html_data) == 2,  grouper(2, itertools.islice(itertools.chain([first_h3], first_h3.next_siblings), 30)))

    for month_tag, table in months_and_tables:
        matched_month = month_year_re.match(month_tag.get_text())
        if not matched_month:
            continue

        month_str, year_str = matched_month.groups()
        year = int(year_str)
        month = months_of_year.index(month_str) + 1

        for row in table.find("tbody").find_all("tr"):
            day_tag, text_tag = row.contents

            dom = int(dom_re.search(day_tag.get_text().strip()).group(0))

            date = datetime.date(year, month, dom)

            events.append((text_tag.get_text(), date))

    # why though
    closed_dates = {date: prettify_key_date_text(text) for (text, date) in events if closed_re.search(text)}

    trimester_ranges = {}
    for tri_n, res in trimester_res.items():
        for valid_regex in filter(lambda regex: regex is not None, res):
            tri_range = tuple([date for (text, date) in events if valid_regex.search(text)])
            if len(tri_range) > 0:
                trimester_ranges[tri_n] = tri_range

    return KeyDates(trimester_ranges=trimester_ranges, closed_dates=closed_dates)
