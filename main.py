#!/usr/bin/env python3

# Copyright (C) 2017  Jamie McClymont, Rhys Davies, and Nick Webster
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

from mytimetable import timetable_scraper
from mytimetable.key_dates_getter import KeyDatesGetter
from mytimetable.determine_current_trimester import determine_current_trimester
from mytimetable import gen_calendar
import argparse
import pytz
import datetime as dt

parser = argparse.ArgumentParser(description='Turn your VUW timetable into an ICS file')
parser.add_argument('timetable')
parser.add_argument('trimester', nargs='?', type=int)
parser.add_argument('--key-dates', default="download")
args = parser.parse_args()

key_dates = KeyDatesGetter(args.key_dates).key_dates
if args.trimester is None:
    args.trimester = determine_current_trimester(key_dates, dt.datetime.now(pytz.timezone("Pacific/Auckland")).date())

trimester = timetable_scraper.scrape(open(args.timetable).read(), args.trimester)

sys.stdout.buffer.write(gen_calendar.make_calendar(trimester, key_dates, args.trimester))
